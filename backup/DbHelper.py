#!/usr/bin/python

import sqlite3
import random
import json



class DbHelper():
	currentConnection = None
	maxPlayers = 2

	def __init__(self,dbPath):
		global currentConnection
		currentConnection = sqlite3.connect(dbPath)
		
	def getCursor(self):
		return currentConnection.cursor()
		
	def safeJsonEncode(self,data):
		if data!=None:
			return json.loads(data)
		else:
			return None
		
	def checkAndReturn(self,statement):
		if (statement!=None):
			return statement[0]
		return None
		
	def getShortGameData(self,gameId):
		cursor = self.getCursor()
		cursor.execute('SELECT shortData FROM games WHERE gameId=?', [gameId])
		return json.loads(self.checkAndReturn(cursor.fetchone()))
		
	def getUserData(self,username):
		cursor = self.getCursor()
		cursor.execute('SELECT data FROM users WHERE username=?', [username])
		return self.checkAndReturn(cursor.fetchone())
		
	def setCurrentTurn(self,gameId,username):
		cursor = self.getCursor()
		cursor.execute('UPDATE lobby SET currentTurn=? WHERE gameId=?',[username,gameId])
	
	def getCurrentTurn(self,gameId):
		cursor = self.getCursor()
		cursor.execute('SELECT currentTurn FROM lobby WHERE gameId=?',[gameId])
		return self.checkAndReturn(cursor.fetchone())
		
	def setUserData(self,username,data):
		cursor = self.getCursor()
		cursor.execute('UPDATE users SET data=? WHERE username=?', [data,username])
		
	def isPlayerAlreadyHasSingleRoom(self,username):
		self.addUser(username)
		cursor = self.getCursor()
		cursor.execute("SELECT * FROM lobby WHERE players=?",[json.dumps([username])])
		return self.checkAndReturn(cursor.fetchone())
		
	def addUser(self,username):
		if (self.getUserData(username)==None):
			currentConnection.execute('INSERT INTO users (username,data) VALUES (?,?)', [username,"[]"])
	
	def addGameToUserData(self,username,gameId):
		data = self.safeJsonEncode(self.getUserData(username))
		data.append(gameId)
		self.setUserData(username,json.dumps(data))
		
	def deleteGameFromUserData(self,username,gameId):
		data = self.safeJsonEncode(self.getUserData(username))
		data.remove(gameId)
		self.setUserData(username,json.dumps(data))
	
	def getGameData(self,gameId):
		cursor = self.getCursor()
		cursor.execute('SELECT data FROM games WHERE gameId=?', [gameId])
		return self.checkAndReturn(cursor.fetchone())
		
	def getGameCode(self,gameId):
		cursor = self.getCursor()
		cursor.execute('SELECT code FROM games WHERE gameId=?', [gameId])
		return self.checkAndReturn(cursor.fetchone())
		
	def setGameData(self,gameId,data):
		code = self.getGameCode(gameId)
		cursor = self.getCursor()
		cursor.execute('UPDATE games SET data=?,code=? WHERE gameId=?', [json.dumps(data),code+1,gameId])
		return code+1
		
	def setGameShortData(self,gameId,shortData):
		cursor = self.getCursor()
		cursor.execute('UPDATE games SET shortData=? WHERE gameId=?', [shortData,gameId])
		
	def addGame(self,gameId,data):
		currentConnection.execute('INSERT INTO games (gameId,data,shortData,code) VALUES (?,?,?,?)', [gameId,data,"{}",0])
		
	def closeConnection(self):
		currentConnection.commit()
		currentConnection.close()
		
	def isRoomExist(self,playerName,opponentName):
		cursor = self.getCursor()
		players1 = [playerName,opponentName]
		players2 = [opponentName,playerName]
		cursor.execute('SELECT gameId FROM lobby WHERE players=? OR players=?', [json.dumps(players1),json.dumps(players2)])
		return self.checkAndReturn(cursor.fetchone())
		
	def deleteGame(self,gameId):
		currentConnection.execute('DELETE FROM games WHERE gameId=?', [gameId])
	
	def getGameId(self):
		currentId = random.randint(1111,1111111111)
		while self.getGameData(currentId) != None:
			currentId = random.randint(1111,1111111111)
		return currentId

	def addRoom(self,owner):
		self.addUser(owner)
		cursor = self.getCursor()
		gameId = self.getGameId()
		self.addGameToUserData(owner,gameId)
		players = [owner]
		cursor.execute('INSERT INTO lobby (players,playersCount,gameId) VALUES (?,?,?)', [json.dumps(players),1,gameId])
		self.addGame(gameId,"{}")
		return gameId
		
	def getRoomPlayers(self,gameId):
		cursor = self.getCursor()
		cursor.execute('SELECT players FROM lobby WHERE gameId=?', [gameId])
		return self.safeJsonEncode(self.checkAndReturn(cursor.fetchone()))

	def deleteRoom(self,gameId):
		self.deleteGame(gameId)
		players = self.getRoomPlayers(gameId);
		for i in range(2):
			self.deleteGameFromUserData(players[i],gameId)
		currentConnection.execute('DELETE FROM lobby WHERE gameId=?', [gameId])
	
	def addPlayerToRoom(self,gameId,player):
		self.addUser(player)
		cursor = self.getCursor()
		self.addGameToUserData(player,gameId)
		currentPlayers = self.getRoomPlayers(gameId)
		currentPlayers.append(player)
		cursor.execute('UPDATE lobby SET players=?,playersCount=? WHERE gameId=?', [json.dumps(currentPlayers),len(currentPlayers),gameId])
		self.setCurrentTurn(gameId,player)
		
	def getFreeRoom(self,player):
		cdata = json.dumps([player])
		cursor = self.getCursor()
		cursor.execute('SELECT gameId FROM lobby WHERE playersCount<? and players<>?',[self.maxPlayers,cdata])
		return self.checkAndReturn(cursor.fetchone())
		
	def getPlayerRooms(self,player):
		data = []
		self.addUser(player)
		games = self.safeJsonEncode(self.getUserData(player))
		for gameId in games:
			roomPlayers = self.getRoomPlayers(gameId)
			currentTurn = self.getCurrentTurn(gameId)
			shortData = self.getShortGameData(gameId)
			room = {}
			room["gameId"] = gameId
			room["players"] = roomPlayers
			room["currentTurn"] = currentTurn
			room["shortData"] = shortData
			data.append(room)
		return data
