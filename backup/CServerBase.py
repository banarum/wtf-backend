#!/usr/bin/python

import cgitb 
import cgi
import json

class CServerBase:
	
	form = None
	onDie = None
	
	def __init__(self,_onDie):
		global form,onDie
		self.onDie = _onDie
		cgitb.enable()
		self.form = cgi.FieldStorage()
	
	def getField(self,name): 
		if name in self.form: 
			return self.form.getvalue(name) 
		else: 
			return None
			
	def getResponseBase(self,code,msg): 
		obj = {} 
		obj["code"] = code 
		obj["msg"] = msg 
		return obj
		
	def sendResponse(self,rsp):
		print("Content-Type: text/html") 
		print()
		print(json.dumps(rsp))
		if (self.onDie):
			self.onDie()
