#!/usr/bin/python

import sys
import hashlib
import DbHelper
import json
from CServerBase import CServerBase
import os

class CServer():
	defaultPath = os.path.dirname(os.path.abspath(__file__))+"/wtf.db"
	dbHelper = None
	
	def __init__(self):
		global dbHelper
		dbHelper = DbHelper.DbHelper(self.defaultPath)


	def onAction(self,action,data):
		returnData = "{}"
		if (action=="join"):
			returnData = self.onJoinEvent(data["username"])
		elif (action=="delete"):
			returnData = self.onDeleteEvent(data["gameId"])
		elif (action=="get"):
			returnData = self.onGetEvent(data["gameId"])
		elif (action=="update"):
			returnData = self.onUpdateEvent(data["gameId"],data["data"],data["username"])
		elif (action=="rooms"):
			returnData = self.onRoomsEvent(data["username"])
		elif (action=="short"):
			returnData = self.onSetShortDataEvent(data["gameId"],data["data"])
		elif (action=="private"):
			returnData = self.onPrivateRoomEvent(data["playerName"],data["opponentName"])
		dbHelper.closeConnection()
		return returnData
	
	def onJoinEvent(self,playerName):
		freeRoom = dbHelper.getFreeRoom(playerName)
		if (freeRoom):
			dbHelper.addPlayerToRoom(freeRoom,playerName)
			return {'msg':'Room joined',"data":{'players':dbHelper.getRoomPlayers(freeRoom),'gameId':freeRoom}}
		else:
			if not(dbHelper.isPlayerAlreadyHasSingleRoom(playerName)):
				freeRoom = dbHelper.addRoom(playerName)
				return {'msg':"Room created","data":{'gameId':freeRoom}}
			else:
				return {'msg':"Room has been already created"}
	
	def onDeleteEvent(self,gameId):
		dbHelper.deleteRoom(gameId)
		return {"msg":"Room Deleted"}
	
	def onGetEvent(self,gameId):
		data = json.loads(dbHelper.getGameData(gameId))
		return {"data":data}
		
	def onUpdateEvent(self,gameId,newData,username):
		code = dbHelper.setGameData(gameId,newData)
		players = dbHelper.getRoomPlayers(gameId)
		usernameOnSet = ""
		for player in players:
			if (player!=username):
				usernameOnSet = player
		dbHelper.setCurrentTurn(gameId,usernameOnSet)
		return {"data":{"versionCode":code}}
		
	def onSetShortDataEvent(self,gameId,data):
		dbHelper.setGameShortData(gameId,data)
		return {"msg":"Short Data Updated"}
		
	def onRoomsEvent(self,username):
		data = dbHelper.getPlayerRooms(username)
		return {"data":data}
		
	def onPrivateRoomEvent(self,playerName,opponentName):
		data = dbHelper.isRoomExist(opponentName,playerName)
		if (data!=None):
			return {"msg":"Room already exist"}
		freeRoom = dbHelper.addRoom(opponentName)
		dbHelper.addPlayerToRoom(freeRoom,playerName)
		return {"msg":"Private room created","code":freeRoom}
		

server = CServer()

srv = CServerBase(None)

hash = hashlib.sha1(("wtfapihash228"+srv.getField("data")).encode("utf-8")).hexdigest()

if (srv.getField("hash")!=hash and False):
	rsp = srv.getResponseBase(200,"Wrong hash")
	srv.sendResponse(rsp)
	sys.exit()

rsp = srv.getResponseBase(200,"Success")



data = server.onAction(srv.getField("action"),json.loads(srv.getField("data")))

if ("msg" in data):
	rsp["msg"] = data["msg"]
	
if ("code" in data):
	rsp["code"] = data["code"]
	
if ("data" in data):
	rsp["data"] = data["data"]

srv.sendResponse(rsp)
