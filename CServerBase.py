#!/usr/bin/python

import cgi
import cgitb
import json


class CServerBase:
    form = None
    on_die = None

    def __init__(self, on_die):
        self.on_die = on_die
        cgitb.enable()
        self.form = cgi.FieldStorage()

    def get_field(self, name):
        if name in self.form:
            return self.form.getvalue(name)
        else:
            return None

    @staticmethod
    def get_response_base(code, msg):
        return {
            "code": code,
            "msg": msg
        }

    @staticmethod
    def pack_response(base, data):
        response = CServerBase.get_response_base(base["code"], base["msg"])

        if "msg" in data:
            response["msg"] = data["msg"]

        if "code" in data:
            response["code"] = data["code"]

        if "data" in data:
            response["data"] = data["data"]
        return response

    def send_response(self, rsp):
        print("Content-Type: text/html")
        print()
        print(json.dumps(rsp))
        if self.on_die:
            self.on_die()
