import json
import os
import time

from flask import Flask, jsonify, send_from_directory
from flask import request
from werkzeug.utils import secure_filename

from CServerBase import CServerBase
from api import CServerWrapper

UPLOAD_FOLDER = '/var/www/Flask/wtf/uploads'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def on_api(hash_sum, action, data):
    server_wrapper = CServerWrapper()
    response = server_wrapper.perform_cmd(hash_sum, action, data)
    return response


def get_extension(filename):
    return filename.rsplit('.', 1)[1].lower()


def allowed_file(filename):
    return '.' in filename and \
           get_extension(filename) in ALLOWED_EXTENSIONS


@app.route("/flask_test")
def hello():
    return "Hello, Welcome to Profitbricks!"

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

def get_file_name(ext):
    return str(round(time.time() * 10000)).replace('.', '') + "." + ext


@app.route("/wtf_upload", methods=['POST'])
def on_upload():
    response = CServerBase.get_response_base(0, "none")
    if 'photo' not in request.files:
        response["code"] = 200
        response["msg"] = "No file attached"
        return jsonify(response)

    file = request.files['photo']

    if file.filename == '':
        response["code"] = 200
        response["msg"] = "No file attached"
        return jsonify(response)
    if file and allowed_file(file.filename):
        filename = secure_filename(get_file_name(get_extension(file.filename)))
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        response["code"] = 200
        response["msg"] = "File successfully uploaded!"
        response["url"] = "http://banarum.com:8080/uploads/"+filename
        return jsonify(response)
    response["code"] = 200
    response["msg"] = "Error occurred"
    return jsonify(response)


@app.route("/wtf_api", methods=["POST", "PUT"])
def wtf_request():
    hash_sum = request.form['hash']
    action = request.form['action']
    data = request.form['data']
    post_obj = on_api(hash_sum, action, data)
    post_data = json.dumps(post_obj)
    if type(post_data) is dict:
        return "{'status':'WTF'}"
    return post_data


if __name__ == "__main__":
    app.run()
