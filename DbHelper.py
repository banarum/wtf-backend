#!/usr/bin/python

import json
import random
import sqlite3


class DbHelper:
    currentConnection = None
    MAX_PLAYERS = 2

    def __init__(self, db_path):
        global currentConnection
        currentConnection = sqlite3.connect(db_path)

    @staticmethod
    def get_cursor():
        return currentConnection.cursor()

    @staticmethod
    def safe_json_encode(data):
        if data:
            return json.loads(data)
        else:
            return None

    @staticmethod
    def check_and_return(statement):
        if statement:
            return statement[0]
        return None

    @staticmethod
    def get_short_game_data(game_id):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT shortData FROM games WHERE gameId=?', [game_id])
        return json.loads(DbHelper.check_and_return(cursor.fetchone()))

    @staticmethod
    def get_user_data(username):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT data FROM users WHERE username=?', [username])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def set_current_turn(game_id, username):
        cursor = DbHelper.get_cursor()
        cursor.execute('UPDATE lobby SET currentTurn=? WHERE gameId=?', [username, game_id])

    @staticmethod
    def get_current_turn(game_id):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT currentTurn FROM lobby WHERE gameId=?', [game_id])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def set_user_data(username, data):
        cursor = DbHelper.get_cursor()
        cursor.execute('UPDATE users SET data=? WHERE username=?', [data, username])

    @staticmethod
    def is_player_already_has_single_room(username):
        DbHelper.add_user(username)
        cursor = DbHelper.get_cursor()
        cursor.execute("SELECT * FROM lobby WHERE players=?", [json.dumps([username])])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def add_user(username):
        if DbHelper.get_user_data(username) is None:
            currentConnection.execute('INSERT INTO users (username,data) VALUES (?,?)', [username, "[]"])

    @staticmethod
    def add_game_to_user_data(username, game_id):
        data = DbHelper.safe_json_encode(DbHelper.get_user_data(username))
        data.append(game_id)
        DbHelper.set_user_data(username, json.dumps(data))

    @staticmethod
    def delete_game_from_user_data(username, game_id):
        data = DbHelper.safe_json_encode(DbHelper.get_user_data(username))
        data.remove(game_id)
        DbHelper.set_user_data(username, json.dumps(data))

    @staticmethod
    def get_game_data(game_id):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT data FROM games WHERE gameId=?', [game_id])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def get_game_code(game_id):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT code FROM games WHERE gameId=?', [game_id])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def set_game_data(game_id, data):
        code = DbHelper.get_game_code(game_id)
        cursor = DbHelper.get_cursor()
        cursor.execute('UPDATE games SET data=?,code=? WHERE gameId=?', [json.dumps(data), code + 1, game_id])
        return code + 1

    @staticmethod
    def set_game_short_data(game_id, short_data):
        cursor = DbHelper.get_cursor()
        cursor.execute('UPDATE games SET shortData=? WHERE gameId=?', [short_data, game_id])

    @staticmethod
    def add_game(game_id, data):
        currentConnection.execute('INSERT INTO games (gameId,data,shortData,code) VALUES (?,?,?,?)',
                                  [game_id, data, "{}", 0])

    @staticmethod
    def close_connection():
        currentConnection.commit()
        currentConnection.close()

    @staticmethod
    def is_room_exist(player_name, opponent_name):
        cursor = DbHelper.get_cursor()
        players1 = [player_name, opponent_name]
        players2 = [opponent_name, player_name]
        cursor.execute('SELECT gameId FROM lobby WHERE players=? OR players=?',
                       [json.dumps(players1), json.dumps(players2)])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def delete_game(game_id):
        currentConnection.execute('DELETE FROM games WHERE gameId=?', [game_id])

    @staticmethod
    def get_game_id():
        current_id = random.randint(1111, 1111111111)
        while DbHelper.get_game_data(current_id):
            current_id = random.randint(1111, 1111111111)
        return current_id

    @staticmethod
    def add_room(owner):
        DbHelper.add_user(owner)
        cursor = DbHelper.get_cursor()
        game_id = DbHelper.get_game_id()
        DbHelper.add_game_to_user_data(owner, game_id)
        players = [owner]
        cursor.execute('INSERT INTO lobby (players,playersCount,gameId) VALUES (?,?,?)',
                       [json.dumps(players), 1, game_id])
        DbHelper.add_game(game_id, "{}")
        return game_id

    @staticmethod
    def get_room_players(game_id):
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT players FROM lobby WHERE gameId=?', [game_id])
        return DbHelper.safe_json_encode(DbHelper.check_and_return(cursor.fetchone()))

    @staticmethod
    def delete_room(game_id):
        DbHelper.delete_game(game_id)
        players = DbHelper.get_room_players(game_id)
        for i in range(2):
            DbHelper.delete_game_from_user_data(players[i], game_id)
        currentConnection.execute('DELETE FROM lobby WHERE gameId=?', [game_id])

    @staticmethod
    def add_player_to_room(game_id, player):
        DbHelper.add_user(player)
        cursor = DbHelper.get_cursor()
        DbHelper.add_game_to_user_data(player, game_id)
        current_players = DbHelper.get_room_players(game_id)
        current_players.append(player)
        cursor.execute('UPDATE lobby SET players=?,playersCount=? WHERE gameId=?',
                       [json.dumps(current_players), len(current_players), game_id])
        DbHelper.set_current_turn(game_id, player)

    @staticmethod
    def get_free_room(player):
        cdata = json.dumps([player])
        cursor = DbHelper.get_cursor()
        cursor.execute('SELECT gameId FROM lobby WHERE playersCount<? and players<>?', [DbHelper.MAX_PLAYERS, cdata])
        return DbHelper.check_and_return(cursor.fetchone())

    @staticmethod
    def get_player_rooms(player):
        data = []
        DbHelper.add_user(player)
        games = DbHelper.safe_json_encode(DbHelper.get_user_data(player))
        for game_id in games:
            room_players = DbHelper.get_room_players(game_id)
            current_turn = DbHelper.get_current_turn(game_id)
            short_data = DbHelper.get_short_game_data(game_id)
            data.append({
                "gameId": game_id,
                "players": room_players,
                "currentTurn": current_turn,
                "shortData": short_data
            })
        return data
