#!/usr/bin/python

import hashlib
import json
import os

from CServerBase import CServerBase
from DbHelper import DbHelper

SECRET_HASH_WORD = "wtfapihash228"

IS_DEBUG = False


class CServer:
    defaultPath = os.path.dirname(os.path.abspath(__file__)) + "/wtf.db"

    def __init__(self):
        DbHelper(self.defaultPath)

    @staticmethod
    def on_action(action, data):
        return_data = "{}"
        if action == "join":
            return_data = CServer.on_join_event(data["username"])
        elif action == "delete":
            return_data = CServer.on_delete_event(data["gameId"])
        elif action == "get":
            return_data = CServer.on_get_event(data["gameId"])
        elif action == "update":
            return_data = CServer.on_update_event(data["gameId"], data["data"], data["username"])
        elif action == "rooms":
            return_data = CServer.on_rooms_event(data["username"])
        elif action == "short":
            return_data = CServer.on_set_short_data_event(data["gameId"], data["data"])
        elif action == "private":
            return_data = CServer.on_private_room_event(data["playerName"], data["opponentName"])
        DbHelper.close_connection()
        return return_data

    @staticmethod
    def on_join_event(player_name):
        free_room = DbHelper.get_free_room(player_name)
        if free_room:
            DbHelper.add_player_to_room(free_room, player_name)
            return {'msg': 'Room joined',
                    "data": {'players': DbHelper.get_room_players(free_room), 'gameId': free_room}}
        else:
            if not (DbHelper.is_player_already_has_single_room(player_name)):
                free_room = DbHelper.add_room(player_name)
                return {'msg': "Room created", "data": {'gameId': free_room}}
            else:
                return {'msg': "Room has been already created"}

    @staticmethod
    def on_delete_event(game_id):
        DbHelper.delete_room(game_id)
        return {"msg": "Room Deleted"}

    @staticmethod
    def on_get_event(game_id):
        data = json.loads(DbHelper.get_game_data(game_id))
        return {"data": data}

    @staticmethod
    def on_update_event(game_id, new_data, username):
        code = DbHelper.set_game_data(game_id, new_data)
        players = DbHelper.get_room_players(game_id)
        username_on_set = ""
        for player in players:
            if player != username:
                username_on_set = player
        DbHelper.set_current_turn(game_id, username_on_set)
        return {"data": {"versionCode": code}}

    @staticmethod
    def on_set_short_data_event(game_id, data):
        DbHelper.set_game_short_data(game_id, data)
        return {"msg": "Short Data Updated"}

    @staticmethod
    def on_rooms_event(username):
        data = DbHelper.get_player_rooms(username)
        return {"data": data}

    @staticmethod
    def on_private_room_event(player_name, opponent_name):
        data = DbHelper.is_room_exist(opponent_name, player_name)
        if data:
            return {"msg": "Room already exist"}
        free_room = DbHelper.add_room(opponent_name)
        DbHelper.add_player_to_room(free_room, player_name)
        return {"msg": "Private room created", "code": free_room}


class CServerWrapper:
    server = None

    def __init__(self):
        global server
        server = CServer()

    @staticmethod
    def verify_hash(data, hash_sum):
        if hash_sum is None or data is None:
            return False
        post_hash_sum = hashlib.sha1((SECRET_HASH_WORD + data).encode("utf-8")).hexdigest()
        return post_hash_sum == hash_sum

    @staticmethod
    def perform_cmd(hash_sum, action, data):
        if not CServerWrapper.verify_hash(data, hash_sum):
            return CServerBase.get_response_base(400, "Wrong hash")

        response_base = CServerBase.get_response_base(200, "Success")
        data_dict = json.loads(data)
        post_data = server.on_action(action, data_dict)
        response = CServerBase.pack_response(response_base, post_data)
        return response